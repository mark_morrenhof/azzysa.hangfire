﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azzysa.Hangfire
{
    public class AzzysaTaskManager
    {
        public static void CancelAll()
        {
            Infostrait.Hangfire.TaskManager.CancelAll();
        }

        public static AzzysaTask GetTask(string taskId)
        {
            var t = Infostrait.Hangfire.TaskManager.GetTask(taskId);

            if (t == null)
                return null;

            var task = JsonConvert.DeserializeObject<AzzysaTask>(t.Details);
            task.AssignTask(t);
            return task;
        }

        public static void Enqueue(System.Linq.Expressions.Expression<Action> methodCall)
        {
            var task = new AzzysaTask("Anonymous Task");
            task.Enqueue(methodCall);
        }
    }
}
