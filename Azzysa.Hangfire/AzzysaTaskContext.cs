﻿using Infostrait.Hangfire;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azzysa.Hangfire
{
    public class AzzysaTaskContext : Infostrait.Hangfire.TaskContext
    {
        [JsonIgnore]
        public AzzysaTask Task => AzzysaTaskManager.GetTask(TaskId);

        public AzzysaTaskContext() : base() { }
        public AzzysaTaskContext(string taskId, TaskParameterCollection parameters) : base(taskId, parameters) { }
    }
}
