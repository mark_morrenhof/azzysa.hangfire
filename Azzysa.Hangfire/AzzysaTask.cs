﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infostrait.Hangfire;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net;

namespace Azzysa.Hangfire
{
    public class AzzysaTask
    {
        private string userId;
        private string notificationEmailAddress;
        private string statusMessage;

        #region Properties
        public string UserId
        {
            get
            {
                return userId;
            }
            set
            {
                userId = value;
                Save();
            }
        }

        public string NotificationEmailAddress
        {
            get
            {
                return notificationEmailAddress;
            }
            set
            {
                notificationEmailAddress = value;
                Save();
            }
        }

        public string StatusMessage
        {
            get
            {
                return statusMessage;
            }
            set
            {
                statusMessage = value;
                Save();
            }
        }
        #endregion

        public AzzysaTask()
        {
            baseTask = new Task();
        }

        public AzzysaTask(string name) : this()
        {
            baseTask.Name = name;
        }

        internal AzzysaTask(Task task)
        {
            baseTask = task;
        }

        private void Save()
        {
            baseTask.Details = JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
        }

        public AzzysaTaskInfo GetInfo() => new AzzysaTaskInfo(this);

        #region References and Redirects to Infostrait.Hangfire.Task
        private Task baseTask;

        [JsonIgnore]
        public Task BaseTask => baseTask;

        public string TaskId => baseTask.TaskId;

        [JsonIgnore]
        public TaskParameterCollection Parameters => baseTask.Parameters;

        [JsonIgnore]
        public AzzysaTaskContext Context => new AzzysaTaskContext(TaskId, Parameters);

        [JsonIgnore]
        public TaskStatus Status => baseTask.Status;

        public short RetryAttempts
        {
            get
            {
                return baseTask.RetryAttempts;
            }
            set
            {
                baseTask.RetryAttempts = value;
            }
        }

        [JsonIgnore]
        public string Name
        {
            get
            {
                return baseTask.Name;
            }
            set
            {
                baseTask.Name = value;
            }
        }

        internal void AssignTask(Task task)
        {
            baseTask = task;
        }

        public void Enqueue(Expression<Action> methodCall)
        {
            baseTask.Enqueue(methodCall);
        }

        public void Cancel()
        {
            baseTask.Cancel();
        }
        #endregion
    }
}
