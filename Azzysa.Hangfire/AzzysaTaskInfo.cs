﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infostrait.Hangfire;

namespace Azzysa.Hangfire
{
    public class AzzysaTaskInfo
    {
        public string TaskId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string StatusMessage { get; set; }
        public decimal? Progress { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ExpireDate { get; set; }

        public AzzysaTaskInfo() { }

        internal AzzysaTaskInfo(AzzysaTask task)
        {
            //Azzysa Task Properties
            TaskId = task.TaskId;
            UserId = task.UserId;
            //Task Properties
            Name = task.BaseTask.Name;
            Status = task.BaseTask.Status.ToString();
            StatusMessage = task.StatusMessage;
            Progress = task.BaseTask.Progress;
            CreationDate = task.BaseTask.CreationDate;
        }
    }
}
