﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infostrait.Hangfire;

namespace Test
{
    public class ApplicationPreload : System.Web.Hosting.IProcessHostPreloadClient
    {
        public void Preload(string[] parameters)
        {
            HangfireBootstrapper.Instance.Start();
        }
    }
}