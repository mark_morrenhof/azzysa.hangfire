﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Infostrait.Hangfire;

[assembly: OwinStartup(typeof(Test.Startup))]

namespace Test
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HangfireConfigurationManager.Configure(app);
        }
    }
}
