﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Test
{
    public class GZipTestHttpHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            using (var fs = File.OpenRead(@"D:\Projects\Workspace\Example 2 (EmptyCollectionContractResolver).json"))
                fs.CopyTo(context.Response.OutputStream);
        }
    }
}