﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="Test.Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:LinkButton id="btnCreateTask1" runat="server" text="create task that runs forever" OnClick="btnCreateTask1_Click"/>
        <br />
        <asp:LinkButton id="btnCreateTask2" runat="server" text="create task that runs for 10 seconds and succeeds" OnClick="btnCreateTask2_Click"/>
        <br />
        <asp:LinkButton id="btnCreateTask3" runat="server" text="create task that runs for 10 seconds and fails" OnClick="btnCreateTask3_Click"/>
        <br />
        <asp:LinkButton id="btnCreateTask4" runat="server" text="create task that runs for 10 seconds, fails and does 1 retry attempt" OnClick="btnCreateTask4_Click"/>
        <br />
        <br />
        <asp:LinkButton ID="btnCancel" runat="server" text="cancel all tasks" OnClick="btnCancel_Click"/>
        <br />
        <br />
        <br />
        <asp:TextBox ID="txtTaskId" Width="230px" runat="server"></asp:TextBox>
        <asp:LinkButton ID="btnCancelTask" runat="server" text="cancel task" OnClick="btnCancelTask_Click"/> | <asp:LinkButton ID="btnGetStatus" runat="server" text="get status" OnClick="btnGetStatus_Click"/>
        <br />
        <br />
        <div style="white-space: pre-wrap;font-family:Consolas;font-size:9pt;"><asp:Literal ID="txtResult" runat="server"></asp:Literal></div>
    </div>
    </form>
</body>
</html>
