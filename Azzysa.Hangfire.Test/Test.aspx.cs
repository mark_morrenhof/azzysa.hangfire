﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Diagnostics;
using Azzysa.Hangfire;
using Newtonsoft.Json;

namespace Test
{
    public class TaskData
    {
        public string UserId;
    }

    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtResult.Text = "";
        }

        protected void btnGetStatus_Click(object sender, EventArgs e)
        {
            string json = GetStatus(txtTaskId.Text);

            txtResult.Text = Server.HtmlEncode(json);
        }

        protected void btnCreateTask1_Click(object sender, EventArgs e)
        {
            var task = new AzzysaTask
            {
                Name = "Task that runs forever",
                UserId = "p.kaneman"
            };

            //todo: works like a charm. Just add code to clean the tasks. Only write data to disk when task is Enqueued...
            //todo: save parameters in database? That would be a bit nicer
            task.Parameters.Add("SomeString", "This is a test");

            task.RetryAttempts = 3;

            task.Enqueue(() => Infinite(task.Context));

            txtResult.Text = $"Task has been enqueued, taskId: {task.BaseTask.TaskId}";
        }

        protected void btnCreateTask2_Click(object sender, EventArgs e)
        {
            var task = new AzzysaTask("Task that runs for 10 seconds and succeeds")
            {
                UserId = "p.kaneman"
            };
            task.Enqueue(() => RunForSpecifiedNumberOfSeconds(task.Context, 10, false));
            txtResult.Text = $"Task has been enqueued, taskId: {task.BaseTask.TaskId}";
        }

        protected void btnCreateTask3_Click(object sender, EventArgs e)
        {
            var task = new AzzysaTask("Task that runs for 10 seconds and fails")
            {
                UserId = "p.kaneman"
            };
            task.Enqueue(() => RunForSpecifiedNumberOfSeconds(task.Context, 10, true));
            txtResult.Text = $"Task has been enqueued, taskId: {task.BaseTask.TaskId}";
        }

        protected void btnCreateTask4_Click(object sender, EventArgs e)
        {
            var task = new AzzysaTask("Task that runs for 10 seconds, fails and does 1 retry attempt")
            {
                UserId = "p.kaneman",
                RetryAttempts = 1
            };
            task.Enqueue(() => RunForSpecifiedNumberOfSeconds(task.Context, 10, true));
            txtResult.Text = $"Task has been enqueued, taskId: {task.BaseTask.TaskId}";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            AzzysaTaskManager.CancelAll();
            txtResult.Text = "Cancellation requests have been made";
        }

        protected void btnCancelTask_Click(object sender, EventArgs e)
        {
            AzzysaTaskManager.GetTask(txtTaskId.Text).Cancel();
            txtResult.Text = "Cancellation request has been made";
        }

        //Make sure IIS is always running configuration: http://docs.hangfire.io/en/latest/deployment-to-production/making-aspnet-app-always-running.html

        public static void Infinite(AzzysaTaskContext c)
        {
            var loop = true;
            var count = 0;

            var test = c.GetParameter<string>("SomeString");

            while (loop)
            {
                //test
                c.ThrowIfCancellationRequested();

                c.Task.NotificationEmailAddress = $"{DateTime.Now:HH:mm:ss}: {count} cycles for task {c.TaskId}, job {c.JobId}";

                Thread.Sleep(1000);
                Debug.WriteLine(c.Task.NotificationEmailAddress);

                count++;
            }
        }

        public static void RunForSpecifiedNumberOfSeconds(AzzysaTaskContext c, int seconds, bool throwError)
        {
            for (int i = 0; i < seconds; i++)
            {
                c.ThrowIfCancellationRequested();

                c.Progress = Math.Round(((i+1) / (decimal)seconds), 5);

                c.Task.NotificationEmailAddress = $"{DateTime.Now:HH:mm:ss}: {i + 1} cycles for task {c.TaskId}, job {c.JobId}, {c.Progress*100}% complete";

            Thread.Sleep(1000);
            }

            c.Progress = 1;

            if (throwError)
                throw new Exception("Some random error occurred for testing purposes");
        }

        public static string GetStatus(string taskId)
        {
            AzzysaTask task = AzzysaTaskManager.GetTask(taskId);

            AzzysaTaskInfo status;

            if (task == null)
            {
                status = new AzzysaTaskInfo();
                status.TaskId = taskId;
                status.Status = "DeletedOrDoesNotExist";
            }
            else
                status = task.GetInfo();

            var json = JsonConvert.SerializeObject(status, Formatting.Indented);

            return json;
        }
    }
}