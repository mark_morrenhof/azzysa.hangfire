﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Owin;
using Hangfire;
using Hangfire.Common;
using Hangfire.Dashboard;

namespace Infostrait.Hangfire
{
    public class HangfireConfigurationManager
    {
        public static void Configure(IAppBuilder app, bool useDashboard = true)
        {
            if (useDashboard)
            {
                var options = new DashboardOptions
                {
                    Authorization = new[]
                    {
                        new LocalRequestsOnlyAuthorizationFilter()
                    }
                };

                app.UseHangfireDashboard("/hangfire", options);
            }

            //app.UseHangfireServer();
        }
    }
}