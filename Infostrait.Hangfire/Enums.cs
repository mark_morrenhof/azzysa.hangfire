﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Hangfire
{
    public enum TaskStatus
    {
        Created = 10,
        Enqueued = 20,
        Scheduled = 30,
        Processing = 40,
        Succeeded = 50,
        Failed = 60,
        Cancelled = 70
    }
}
