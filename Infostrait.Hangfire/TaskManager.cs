﻿using Hangfire;
using Hangfire.Client;
using Hangfire.Common;
using Hangfire.Server;
using Hangfire.States;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire.Storage;

using hf = Hangfire;
using Hangfire.Logging;
using System.Threading;
using System.Data.Linq;

namespace Infostrait.Hangfire
{
    public class TaskManager
    {
        private static Timer timer;

        #region Properties
        internal static TaskCollection Tasks { get; private set; }
        #endregion

        static TaskManager()
        {
            try
            {
                using (var dc = new DataContext())
                    Tasks = new TaskCollection(from a in dc.Tasks select a);
            }
            catch
            {
                //If app fails to start this will crash on close
            }
        }

        #region Task Expiration
        public static void StartCheckingTaskExpiration()
        {
            timer = new Timer(TimerCallback, null, 0, 10 * 1000);
        }

        public static void StopCheckingTaskExpiration()
        {
            if (timer == null)
                return;

            timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        private static void TimerCallback(object state)
        {
            List<Task> toDelete = new List<Task>();

            foreach(Task task in Tasks.Values)
            {
                if (task.ExpireAt != null && task.ExpireAt < DateTime.UtcNow)
                    toDelete.Add(task);
            }

            foreach (Task task in toDelete)
                Tasks.Remove(task.TaskId);

            using (var dc = new DataContext())
            {
                dc.ExecuteCommand("delete p from TaskParameter p left join Task t  on p.TaskId = t.TaskId where ExpireAt < getutcdate() or t.TaskId is null");
                dc.ExecuteCommand("delete from task where ExpireAt < getutcdate()");
            }
        }
        #endregion

        public static Task GetTask(string taskId)
        {
            if(Tasks.ContainsKey(taskId))
                return Tasks[taskId];
            return null;
        }

        public static void Add(Task task)
        {
            Tasks.Add(task.TaskId, task);

            using (var dc = new DataContext())
            {
                try
                {
                    dc.Tasks.InsertOnSubmit(task);
                    dc.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    foreach (ObjectChangeConflict occ in dc.ChangeConflicts)
                        occ.Resolve(RefreshMode.KeepChanges);
                }
            }

            task.Parameters.Persist();
        }

        public static void ThrowIfCancellationRequested(TaskContext context)
        {
            if (!context.IsCancellationRequested)
                return;

            throw new JobAbortedException();
        }

        internal static void CancelTask(string taskId)
        {
            CancelTask(Tasks[taskId]);
        }

        internal static void CancelTask(Task task)
        {
            using (var dc = new DataContext())
            {
                try
                {
                    dc.States.InsertOnSubmit(new State
                    {
                        JobId = int.Parse(task.JobId),
                        Name = "Cancelled",
                        Reason = null,
                        CreatedAt = DateTime.UtcNow,
                        Data = JsonConvert.SerializeObject(new { CancelledAt = DateTime.Now, OtherDetails = "..." })
                    });
                    dc.SubmitChanges();
                }
                catch (ChangeConflictException)
                {
                    foreach (ObjectChangeConflict occ in dc.ChangeConflicts)
                        occ.Resolve(RefreshMode.KeepChanges);
                }
            }

            task.Status = TaskStatus.Cancelled;

            BackgroundJob.Delete(task.JobId);
        }

        public static void CancelAll()
        {
            foreach (var task in Tasks.Values)
                task.Cancel();
        }
    }

    //This is a clone of the Hangfire.AutomaticRetryAttribute, but implemented with out own Retry Attempt logic and other functionality
    public sealed class TaskManagerAttribute : JobFilterAttribute, IElectStateFilter, IApplyStateFilter, IServerFilter
    {
        private static readonly ILog Logger = LogProvider.For<AutomaticRetryAttribute>();

        private readonly object _lockObject = new object();
        //private AttemptsExceededAction _onAttemptsExceeded;
        private bool _logEvents = true;

        /// <inheritdoc />
        public void OnPerforming(PerformingContext context)
        {
            Task task = GetTask(context.BackgroundJob.Id);

            if (task != null)
                task.Status = TaskStatus.Processing;
        }

        /// <inheritdoc />
        public void OnPerformed(PerformedContext context)
        {
            Task task = GetTask(context.BackgroundJob.Id);

            if (task == null)
                return;

            if (context.Exception == null)
            {
                task.Status = TaskStatus.Succeeded;
            }
            else if (context.Exception.GetType() == typeof(JobAbortedException))
            {
                TaskManager.CancelTask(task);
            }
            else
            {
                task.Status = TaskStatus.Failed;
                task.Exception = context.Exception;
            }
        }

        /// <inheritdoc />
        public void OnStateElection(ElectStateContext context)
        {
            var failedState = context.CandidateState as FailedState;
            if (failedState == null)
                return;

            Task task = GetTask(context.BackgroundJob.Id);

            if (task == null)
                return;

            var retryAttempt = context.GetJobParameter<int>("RetryCount") + 1;

            if (retryAttempt <= task.RetryAttempts)
                ScheduleAgainLater(context, retryAttempt, failedState);
            //todo: Make Configurable
            //else if (retryAttempt > task.RetryAttempts && OnAttemptsExceeded == AttemptsExceededAction.Delete)
            //{
            //    TransitionToDeleted(context, failedState);
            //}
            else
            {
                if (_logEvents)
                    Logger.ErrorException($"Failed to process the job '{context.BackgroundJob.Id}': an exception occurred.", failedState.Exception);
            }
        }

        /// <inheritdoc />
        public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            if (context.NewState is ScheduledState && context.NewState.Reason != null && context.NewState.Reason.StartsWith("Retry attempt"))
                transaction.AddToSet("retries", context.BackgroundJob.Id);

            if (context.NewState is SucceededState || context.NewState is DeletedState)
            {
                Task task = GetTask(context.BackgroundJob.Id);

                if(task != null)
                    task.ExpireAt = DateTime.UtcNow.Add(context.JobExpirationTimeout);
            }
            else if ((context.OldStateName == SucceededState.StateName || context.OldStateName == FailedState.StateName || context.OldStateName == DeletedState.StateName) && context.NewState is EnqueuedState)
            {
                Task task = GetTask(context.BackgroundJob.Id);

                if (task != null)
                {
                    task.IsCancellationRequested = false;
                    task.RetryCount = 0;
                    task.ExpireAt = null;
                }
            }
        }

        /// <inheritdoc />
        public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            if (context.OldStateName == ScheduledState.StateName)
                transaction.RemoveFromSet("retries", context.BackgroundJob.Id);
        }

        /// <summary>
        /// Schedules the job to run again later. See <see cref="SecondsToDelay"/>.
        /// </summary>
        /// <param name="context">The state context.</param>
        /// <param name="retryAttempt">The count of retry attempts made so far.</param>
        /// <param name="failedState">Object which contains details about the current failed state.</param>
        private void ScheduleAgainLater(ElectStateContext context, int retryAttempt, FailedState failedState)
        {
            Task task = GetTask(context.BackgroundJob.Id);

            if (task == null)
                return;

            context.SetJobParameter("RetryCount", retryAttempt);

            var delay = TimeSpan.FromSeconds(SecondsToDelay(retryAttempt));

            const int maxMessageLength = 50;
            var exceptionMessage = failedState.Exception.Message.Length > maxMessageLength ? failedState.Exception.Message.Substring(0, maxMessageLength - 1) + "…" : failedState.Exception.Message;

            // If attempt number is less than max attempts, we should schedule the job to run again later.
            context.CandidateState = new ScheduledState(delay)
            {
                Reason = $"Retry attempt {retryAttempt} of {task.RetryAttempts}: {exceptionMessage}"
            };

            task.Status = TaskStatus.Scheduled;

            if (_logEvents)
                Logger.WarnException($"Failed to process the job '{context.BackgroundJob.Id}': an exception occurred. Retry attempt {retryAttempt} of {task.RetryAttempts} will be performed in {delay}.", failedState.Exception);
        }

        /// <summary>
        /// Transition the candidate state to the deleted state.
        /// </summary>
        /// <param name="context">The state context.</param>
        /// <param name="failedState">Object which contains details about the current failed state.</param>
        private void TransitionToDeleted(ElectStateContext context, FailedState failedState)
        {
            Task task = GetTask(context.BackgroundJob.Id);

            if (task == null)
                return;

            context.CandidateState = new DeletedState
            {
                Reason = task.RetryAttempts > 0 ? "Exceeded the maximum number of retry attempts." : "Retries were disabled for this job."
            };

            if (_logEvents)
                Logger.WarnException($"Failed to process the job '{context.BackgroundJob.Id}': an exception occured. Job was automatically deleted because the retry attempt count exceeded {task.RetryAttempts}.", failedState.Exception);
        }

        // delayed_job uses the same basic formula
        private static int SecondsToDelay(long retryCount)
        {
            var random = new Random();
            return (int)Math.Round(
                Math.Pow(retryCount - 1, 4) + 15 + random.Next(30) * retryCount);
        }

        private Task GetTask(string jobId)
        {
            foreach (Task task in TaskManager.Tasks.Values.Where(t => t.JobId != null))
            {
                    if (task.JobId.Equals(jobId))
                        return task;
            }

            return null;
        }
    }
}
