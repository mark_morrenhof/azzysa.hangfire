﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Infostrait.Hangfire
{
    /// <summary>
    /// Custom JsonConvert so that the TaskId can remain private, but still have the ability to deserialize the object
    /// </summary>
    public class TaskContextConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(TaskContext).IsAssignableFrom(objectType);
        }

        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null;

            if (!CanConvert(objectType))
                return null;

            JObject jObject = JObject.Load(reader);

            var taskId = jObject["TaskId"].Value<string>();

            TaskParameterCollection parameters = TaskParameterCollection.Create(taskId);

            if (jObject["Parameters"] != null)
            {
                foreach (JValue p in jObject["Parameters"])
                    parameters.Add(p.Value<string>());
            }

            return Activator.CreateInstance(objectType, taskId, parameters);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var context = value as TaskContext;

            writer.WriteStartObject();

            writer.WritePropertyName("TaskId");
            writer.WriteValue(context.TaskId);

            if (context?.Parameters.Count > 0)
            {
                writer.WritePropertyName("Parameters");
                writer.WriteStartArray();

                foreach (var name in context.Parameters)
                    writer.WriteValue(name);

                writer.WriteEndArray();
            }

            writer.WriteEndObject();
        }
    }
}
