﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Hangfire.Exceptions
{
    public class TaskNotFoundException : Exception
    {
        public TaskNotFoundException(string taskId) : base($"Could not find a task with TaskId '{taskId}'")
        {

        }
    }
}
