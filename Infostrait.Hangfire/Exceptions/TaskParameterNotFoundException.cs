﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Hangfire.Exceptions
{
    public class TaskParameterNotFoundException : Exception
    {
        public TaskParameterNotFoundException(string taskId, string name) : base($"Could not find parameter '{name}' for task with TaskId: '{taskId}'!")
        {

        }
    }
}
