﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Hangfire
{
    public class TaskCollection : Dictionary<string, Task>
    {
        public TaskCollection() { }

        public TaskCollection(IEnumerable<Task> range)
        {
            foreach (var t in range)
                Add(t.TaskId, t);
        }
    }
}
