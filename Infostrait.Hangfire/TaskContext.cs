﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Infostrait.Hangfire
{
    [JsonConverter(typeof(TaskContextConverter))]
    public class TaskContext
    {
        private string taskId;
        private TaskParameterCollection parameters;

        #region Properties
        public string TaskId => taskId;
        public TaskParameterCollection Parameters => parameters;

        [JsonIgnore]
        public string JobId => TaskManager.Tasks[TaskId].JobId;
        [JsonIgnore]
        public bool IsCancellationRequested => TaskManager.Tasks[TaskId].IsCancellationRequested;

        [JsonIgnore]
        public decimal? Progress
        {
            get
            {
                return TaskManager.Tasks[TaskId].Progress;
            }
            set
            {
                TaskManager.Tasks[TaskId].Progress = value;
            }
        }
        #endregion

        public TaskContext() : this(Guid.Empty.ToString("n"), new TaskParameterCollection(Guid.Empty.ToString("n")))
        {
        }

        public TaskContext(string taskId, TaskParameterCollection parameters)
        {
            this.taskId = taskId;
            this.parameters = parameters;
        }

        public void ThrowIfCancellationRequested()
        {
            TaskManager.ThrowIfCancellationRequested(this);
        }

        public T GetParameter<T>(string key)
        {
            return Parameters.GetData<T>(key);
        }
    }
}