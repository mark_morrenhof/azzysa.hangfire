﻿USE [Hangfire]
GO

/****** Object:  Table [dbo].[Task]    Script Date: 8-9-2016 16:26:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Task](
	[JobId] [varchar](50) NOT NULL,
	[TaskId] [varchar](50) NULL,
	[Name] [varchar](max) NULL,
	[Progress] [decimal](6, 5) NULL,
	[Status] [smallint] NOT NULL,
	[RetryAttempts] [smallint] NOT NULL,
	[RetryCount] [smallint] NOT NULL,
	[IsCancellationRequested] [bit] NOT NULL,
	[Details] [text] NULL,
	[CreationDate] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

USE [Hangfire]
GO

/****** Object:  Table [dbo].[TaskParameter]    Script Date: 28-9-2016 10:45:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TaskParameter](
	[TaskId] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Value] [varbinary](max) NULL,
 CONSTRAINT [PK_TaskParameter] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO



