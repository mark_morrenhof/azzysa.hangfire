﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Hangfire
{
    public class TaskParameterCollection : List<string>
    {
        private Dictionary<string, object> temp;
        private bool isTemp = true;

        public string TaskId { get; set; }

        public TaskParameterCollection(string taskId)
        {
            temp = new Dictionary<string, object>();

            TaskId = taskId;
        }

        internal static TaskParameterCollection Create(string taskId)
        {
            return new TaskParameterCollection(taskId)
            {
                temp = null,
                isTemp = false
            };
        }

        public void Add(string name, object value)
        {
            if (isTemp)
                temp[name] = value;
            else
                SaveObjectToDatabase(name, value);

            Add(name);
        }

        public T GetData<T>(string name)
        {
            if (!Contains(name))
                throw new Exceptions.TaskParameterNotFoundException(TaskId, name);

            return GetObject<T>(name);
        }

        private T GetObject<T>(string name)
        {
            if (isTemp)
                return (T)Convert.ChangeType(temp[name], typeof(T));

            using (var dc = new DataContext())
            {
                var parameter = (from p in dc.TaskParameters where p.TaskId == TaskId && p.Name == name select p).SingleOrDefault();

                if(parameter == null)
                    throw new Exceptions.TaskParameterNotFoundException(TaskId, name);

                using (var input = new MemoryStream(parameter.Value.ToArray()))
                using (var output = new MemoryStream())
                {
                    using (var gzip = new GZipStream(input, CompressionMode.Decompress))
                        gzip.CopyTo(output);

                    var json = Encoding.UTF8.GetString(output.ToArray());
                    return JsonConvert.DeserializeObject<T>(json);
                }
            }
        }

        private void SaveObjectToDatabase(string name, object value)
        {
            using (var dc = new DataContext())
            {
                var json = JsonConvert.SerializeObject(value, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore, ContractResolver = new EmptyCollectionContractResolver() });
                var bytes = Encoding.UTF8.GetBytes(json);

                using (var input = new MemoryStream(bytes))
                using (var output = new MemoryStream())
                {
                    try
                    {
                        using (var gzip = new GZipStream(output, CompressionMode.Compress))
                            input.CopyTo(gzip);

                        var parameter = new TaskParameter
                        {
                            TaskId = TaskId,
                            Name = name,
                            Value = output.ToArray()
                        };

                        dc.TaskParameters.InsertOnSubmit(parameter);
                        dc.SubmitChanges();
                    }
                    catch (ChangeConflictException)
                    {
                        foreach (ObjectChangeConflict occ in dc.ChangeConflicts)
                            occ.Resolve(RefreshMode.KeepChanges);
                    }
                }
            }
        }

        internal void Persist()
        {
            isTemp = false;

            foreach (string key in temp.Keys)
                SaveObjectToDatabase(key, temp[key]);

            temp = null;
        }

        //File System Implementation
        /*private static string path;

        public string TaskId { get; set; }

        static TaskParameterCollection()
        {
            path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Cache");
        }

        public TaskParameterCollection(string taskId)
        {
            TaskId = taskId;
        }

        public void Add(string name, object value)
        {
            var key = SetObject(value);

            base.Add(name, key);
        }

        public T GetData<T>(string name)
        {
            var key = base[name];

            if (key == null)
                return default(T);

            return GetObject<T>(key);
        }


        private T GetObject<T>(string key)
        {
            var filePath = Path.Combine(path, TaskId, $"{key}.json.gz");

            if (!File.Exists(filePath))
                throw new FileNotFoundException($"Can't find file '{filePath}'!");

            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            using (GZipStream gz = new GZipStream(fs, CompressionMode.Decompress))
            {
                using (var sr = new StreamReader(gz))
                {
                    var json = sr.ReadToEnd();
                    return JsonConvert.DeserializeObject<T>(json);
                }
            }
        }

        private string SetObject(object value)
        {
            var info = GetFileInfo();
            var json = JsonConvert.SerializeObject(value, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore, ContractResolver = new EmptyCollectionContractResolver() });

            if (!info.Directory.Exists)
                info.Directory.Create();

            var bytes = Encoding.UTF8.GetBytes(json);

            using (FileStream fs = new FileStream(info.FullName, FileMode.CreateNew))
            using (GZipStream zipStream = new GZipStream(fs, CompressionMode.Compress, false))
                zipStream.Write(bytes, 0, bytes.Length);

            return Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(info.Name)); //twice to remove .json.gz
        }

        private FileInfo GetFileInfo()
        {
            var filename = Path.GetRandomFileName();

            filename = Path.ChangeExtension(filename, "json.gz");

            var filePath = new FileInfo(Path.Combine(path, TaskId, filename));

            if (filePath.Exists)
                return GetFileInfo();
            return filePath;
        }*/
    }
}
