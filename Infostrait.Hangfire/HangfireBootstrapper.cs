﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using Hangfire;
using Hangfire.Common;

namespace Infostrait.Hangfire
{
    public class HangfireBootstrapper : IRegisteredObject
    {
        public static readonly HangfireBootstrapper Instance = new HangfireBootstrapper();
        private static string connectionStringName = "HangfireConnection";

        private readonly object _lockObject = new object();
        private bool _started;

        private BackgroundJobServer _backgroundJobServer;

        public static string ConnectionStringName
        {
            get
            {
                return connectionStringName;
            }
            set
            {
                connectionStringName = value;
            }
        }

        private HangfireBootstrapper()
        {
        }

        public void Start()
        {
            lock (_lockObject)
            {
                if (_started)
                    return;
                _started = true;

                HostingEnvironment.RegisterObject(this);

                #region Remove Default Automatic Retry Filter
                JobFilter automaticRetry = null;

                foreach (var filter in GlobalJobFilters.Filters)
                {
                    if (filter.Instance is AutomaticRetryAttribute)
                    {
                        automaticRetry = filter;
                        break;
                    }
                }

                GlobalJobFilters.Filters.Remove(automaticRetry?.Instance); // Remove AutomaticRetryAttribute since TaskManagerAttribute handles that work
                GlobalJobFilters.Filters.Add(new TaskManagerAttribute());
                #endregion

                DataContext.ConnectionStringName = connectionStringName;
                GlobalConfiguration.Configuration.UseSqlServerStorage(connectionStringName);
                DatabaseManager.Verify();

                _backgroundJobServer = new BackgroundJobServer();
                
                TaskManager.StartCheckingTaskExpiration();
            }
        }

        public void Stop()
        {
            lock (_lockObject)
            {
                if (_backgroundJobServer != null)
                    _backgroundJobServer.Dispose();

                TaskManager.StopCheckingTaskExpiration();

                HostingEnvironment.UnregisterObject(this);

                _started = false;
            }
        }

        void IRegisteredObject.Stop(bool immediate)
        {
            Stop();
        }
    }
}
