﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Hangfire.Annotations;
using Hangfire.Server;
using Hangfire.SqlServer;
using Newtonsoft.Json;
using Hangfire;
using Hangfire.Common;

using hf = Hangfire;
using System.Data.Linq;

namespace Infostrait.Hangfire
{
    public partial class Task
    {
        public TaskContext Context { get; private set; }

        public TaskParameterCollection Parameters => Context.Parameters;

        internal Exception Exception { get; set; }

        public Task(string name) : this()
        {
            Name = name;
        }

        partial void OnCreated()
        {
            IgnoreChanges = true;

            _CreationDate = DateTime.Now;

            TaskId = Guid.NewGuid().ToString("n");
            Context = new TaskContext(TaskId, new TaskParameterCollection(TaskId));
            Status = TaskStatus.Created;
            IsCancellationRequested = false;
            RetryAttempts = 0;

            IgnoreChanges = false;
        }

        private bool IgnoreChanges = false;

        #region Save Changes

        #region Progress
        partial void OnJobIdChanged()
        {
            SaveProperty(delegate (Task task) {
                task.JobId = JobId;
            });
        }
        #endregion

        #region Progress
        partial void OnProgressChanged()
        {
            SaveProperty(delegate (Task task) {
                task.Progress = Progress;
            });
        }
        #endregion

        #region Status
        partial void OnStatusChanged()
        {
            SaveProperty(delegate (Task task) {
                task.Status = Status;
            });
        }
        #endregion

        #region RetryAttempts
        partial void OnRetryAttemptsChanged()
        {
            SaveProperty(delegate (Task task) {
                task.RetryAttempts = RetryAttempts;
            });
        }
        #endregion

        #region RetryCount
        partial void OnRetryCountChanged()
        {
            SaveProperty(delegate (Task task) {
                task.RetryCount = RetryCount;
            });
        }
        #endregion

        #region IsCancellationRequested
        partial void OnIsCancellationRequestedChanged()
        {
            SaveProperty(delegate (Task task) {
                task.IsCancellationRequested = IsCancellationRequested;
            });
        }
        #endregion

        #region Details
        partial void OnDetailsChanged()
        {
            SaveProperty(delegate (Task task) {
                task.Details = Details;
            });
        }
        #endregion

        #region ExpireAt
        partial void OnExpireAtChanged()
        {
            SaveProperty(delegate (Task task) {
                task.ExpireAt = ExpireAt;
            });
        }
        #endregion

        private delegate void TaskAction(Task task);

        private void SaveProperty(TaskAction method)
        {
            if (IgnoreChanges)
                return;


            using (var dc = new DataContext())
            {
                try
                {
                    var t = (from tasks in dc.Tasks
                             where tasks.TaskId == TaskId
                             select tasks).SingleOrDefault();

                    if (t == null)
                        return;

                    t.IgnoreChanges = true;

                    method.Invoke(t);

                    t.IgnoreChanges = false;

                    dc.SubmitChanges(ConflictMode.ContinueOnConflict);
                }
                catch (ChangeConflictException)
                {
                    foreach (ObjectChangeConflict occ in dc.ChangeConflicts)
                        occ.Resolve(RefreshMode.KeepChanges);
                }
            }
        }

        #endregion

        public void Enqueue([InstantHandle] [NotNull] Expression<Action> methodCall)
        {
            TaskManager.Add(this);
            JobId = BackgroundJob.Enqueue(methodCall);
            Status = TaskStatus.Enqueued;
        }

        public void Cancel()
        {
            switch (Status)
            {
                case TaskStatus.Created:
                case TaskStatus.Enqueued:
                case TaskStatus.Scheduled:
                    //IsCancellationRequested is set just in case the task starts processing before the job is deleted
                    IsCancellationRequested = true;
                    TaskManager.CancelTask(this);
                    break;
                case TaskStatus.Processing:
                    IsCancellationRequested = true;
                    break;
                case TaskStatus.Failed:
                case TaskStatus.Cancelled:
                case TaskStatus.Succeeded:
                default:
                    //Do Nothing
                    break;
            }
        }
    }
}
