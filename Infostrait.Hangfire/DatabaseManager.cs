﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Infostrait.Hangfire
{
    public class DatabaseManager
    {
        public static void Verify()
        {
            using (var sqlConn = new SqlConnection(DataContext.ConnectionString))
            using (var cmd = new SqlCommand("select * from information_schema.tables", sqlConn))
            {
                List<string> tables = new List<string>();

                sqlConn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                        tables.Add((string)dr["TABLE_NAME"]);
                }

                if (!tables.Contains("Job"))
                    throw new InvalidOperationException("Hangfire tables have not been created yet, Infostrait.Hangfire.DatabaseManager can't continue!");

                if (!tables.Contains("Task"))
                    CreateTable(sqlConn, typeof(Task));

                if (!tables.Contains("TaskParameter"))
                    CreateTable(sqlConn, typeof(TaskParameter));
            }
        }

        private static void CreateTable(SqlConnection sqlConn, Type type)
        {
            if (!type.IsDefined(typeof(System.Data.Linq.Mapping.TableAttribute)))
                throw new NotSupportedException($"Can't create table of '{type.FullName}' because it doesn't contain a TableAttribute!");

            string full_name = type.GetCustomAttribute<System.Data.Linq.Mapping.TableAttribute>().Name;

            string table = full_name;
            string schema = null;

            if (full_name.Contains("."))
            {
                schema = full_name.Substring(0, full_name.IndexOf('.'));

                if (schema.StartsWith("["))
                    schema = schema.Substring(1);
                if (schema.EndsWith("]"))
                    schema = schema.Substring(0, schema.Length - 1);

                table = full_name.Substring(full_name.IndexOf('.') + 1);

                if (table.StartsWith("["))
                    table = table.Substring(1);
                if (table.EndsWith("]"))
                    table = table.Substring(0, table.Length - 1);
            }

            List<string> keys = new List<string>();
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"CREATE TABLE {(schema != null ? $"[{schema}]." : "")}[{table}] (");

            bool first = true;

            foreach (var info in type.GetProperties())
            {
                if (!info.IsDefined(typeof(System.Data.Linq.Mapping.ColumnAttribute), false))
                    continue;

                System.Data.Linq.Mapping.ColumnAttribute attribute = info.GetCustomAttribute<System.Data.Linq.Mapping.ColumnAttribute>();

                string column = attribute.Storage.StartsWith("_") ? attribute.Storage.Substring(1) : attribute.Storage;
                string dbType = attribute.DbType;

                if(first)
                    sb.AppendLine($"    [{column}] {dbType}");
                else
                    sb.AppendLine($"   ,[{column}] {dbType}");

                if (attribute.IsPrimaryKey)
                    keys.Add(column);

                first = false;
            }

            if (keys.Count > 0)
            {
                sb.AppendLine($"   ,CONSTRAINT [PK_{table}] PRIMARY KEY CLUSTERED");
                sb.AppendLine($"    (");

                first = true;

                foreach (var key in keys)
                {
                    if(first)
                        sb.AppendLine($"        [{key}] ASC");
                    else
                        sb.AppendLine($"       ,[{key}] ASC");

                    first = false;
                }

                sb.AppendLine($"    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]");
                sb.AppendLine($") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]");
            }
            else
                sb.AppendLine($")");

            using (var cmd = new SqlCommand(sb.ToString(), sqlConn))
                cmd.ExecuteNonQuery();
        }
    }
}
