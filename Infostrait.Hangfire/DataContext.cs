﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Hangfire
{
    public class DataContext : HangfireDataContext
    {
        public static string ConnectionStringName;

        public static string ConnectionString => System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;

        public DataContext() : base(ConnectionString) { }
    }
}
