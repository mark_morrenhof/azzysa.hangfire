﻿using Hangfire.Annotations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Hangfire
{
    public class Task<T>
    {
        private Task task;
        private T _data;

        public TaskContext Context => task.Context;

        public T Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
                task.Details = JsonConvert.SerializeObject(Data, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
            }
        }

        public Task()
        {
            task = new Task();
            Data = Activator.CreateInstance<T>();
        }

        public Task(string name) : this()
        {
            task.Name = name;
        }

        public void Enqueue([InstantHandle] [NotNull] Expression<Action> methodCall)
        {
            task.Details = JsonConvert.SerializeObject(Data, Formatting.None, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
            task.Enqueue(methodCall);
        }
    }
}
